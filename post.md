Approach and Process
What in my process and approach to this project would I do differently next time?
- Study the API better, plan and execute. Don't dwell to much one stuff.

What in my process and approach to this project went well that I would repeat next time?
- Dig into the rabbit hole deeper.

Code and Code Design
What in my code and program design in the project would I do differently next time?
- Better folder structure,
- Plan, sketch and organise my components properly.
https://reactjs.org/docs/thinking-in-react.html
- Got confuse with CSS quite a couple of times, keep going into the wrong file

What in my code and program design in the project went well? Is there anything I would do the same next time?
- console.log always
- Trust in bootstrap, have faith in it

For each, please include code examples.

Code snippet up to 20 lines.
Code design documents or architecture drawings / diagrams.

    // Pass props from child component {NavLists} to parent component {App}
    categoryInfo(category){
        // console.log('hello from SiteNavbar')
        // console.log(category)
        this.props.categoryInfo(category);
    }

        return(
            <Nav className="main-navigation">
                {/* Getting the title of each category */}
                {this.state.categories.map((list) =>
                    <NavDropdown title={this.jsUcfirst(list.slug)} id="basic-nav-dropdown">
                        {/* Getting the list of sub-categories from each category */}
                        {list.children.map((child) =>
                            <NavDropdown.Item onClick={this.handleCategory} id={child.slug}>
                            {child.name}
                            </NavDropdown.Item>
                        )}
                    </NavDropdown>
                )}
            </Nav>
            )k



WDI Unit 4 Post Mortem
What habits did I use during this unit that helped me?
- Never sleep, drink coffee work at night

What habits did I have during this unit that I can improve on?
- Having a proper routine, console log properly

How is the overall level of the course overall? (instruction, course materials, etc.)
- Okay



--- Try to read about REDUX
--- react contacts
--- style components *****
--- and jss