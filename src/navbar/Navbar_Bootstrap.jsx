import React from 'react';
import axios from 'axios'
// import BOOTSTRAP COMPONENTS
import Col from 'react-bootstrap/Col'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
// import CSS
import './Navbar.css';
import { MdAddShoppingCart } from "react-icons/md";
import { IoMdSearch } from "react-icons/io";




// Parent Component
class SiteNavbar extends React.Component {
    constructor(){
        super();
        this.state = {
            logoLink: "https://images.nekojam.com/wp-content/uploads/2019/01/23123239/Nekojam-White-Logo.png",
            homeLink: "https://nekojam.com/"
        }
        this.categoryInfo = this.categoryInfo.bind(this)
    }
    // Pass props from child component {NavLists} to parent component {App}
    categoryInfo(category){
        // console.log('hello from SiteNavbar')
        // console.log(category)
        this.props.categoryInfo(category);
    }
    render(){
        return(
                <Navbar sticky="top" collapseOnSelect className="bgColor siteHeader" >

                    {/* Column for Logo*/}
                    <Col lg="auto">
                        <Navbar.Brand>
                            <SiteLogo homeLink={this.state.homeLink} logoLink={this.state.logoLink}/>
                        </Navbar.Brand>
                    </Col>

                    {/* Column for Navigation*/}
                    <Col className="d-none d-lg-block" >
                        <NavLists categoryInfo={this.categoryInfo} />
                    </Col>

                    {/* Column for Hamburger + Side Menu*/}
                    <Col lg="auto">
                        <Nav as='ul'>
                            <Nav.Item as="li" className="fa-icons icon-search"><IoMdSearch /></Nav.Item>
                            <Nav.Item as="li" className="fa-icons"><i className="far fa-heart"></i></Nav.Item>
                            <Nav.Item
                              as="li"
                              className="fa-icons icon-cart"
                              onClick={this.props.modalClick}
                              >
                              <MdAddShoppingCart  />
                            </Nav.Item>
                            <Nav.Item as="li" className="fa-icons"><i className="far fa-user"></i></Nav.Item>
                            <Nav.Item as="li"><HamburgerIcon /></Nav.Item>
                        </Nav>
                    </Col>
                </Navbar>
            )
    }
}

// Stateless Component | Logo
class SiteLogo extends React.Component {
    render() {
        return(
                <a href={this.props.homeLink} rel="home">
                    <img src={this.props.logoLink} alt=""/>
                </a>
            )
    }
}

// Navigation Component
class NavLists extends React.Component {
    constructor(){
        super();
        this.state = {
            categories: []
        }

        axios.get("https://shop.nekojam.com/api/categories/")
            .then(result => {
                this.setState({
                    categories: result.data
                });
            });

        this.handleCategory = this.handleCategory.bind(this)
    }

    // Pass props to parent component {SiteNavBar} above
    handleCategory(event){
        const path = event.target.id;
        this.props.categoryInfo(path);
    }

    // Make the first letter of slug key Uppercase
    jsUcfirst(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    render() {
        return(
            <Nav className="main-navigation">
                {/* Getting the title of each category */}
                {this.state.categories.map((list) =>
                    <NavDropdown title={this.jsUcfirst(list.slug)} id="basic-nav-dropdown">
                        {/* Getting the list of sub-categories from each category */}
                        {list.children.map((child) =>
                            <NavDropdown.Item onClick={this.handleCategory} id={child.slug}>
                            {child.name}
                            </NavDropdown.Item>
                        )}
                    </NavDropdown>
                )}
            </Nav>
            )
    }
}

class HamburgerIcon extends React.Component {
    constructor(){
        super();
        this.state = {
            active: false
        }
        this.onToggleIcon = this.onToggleIcon.bind( this );
    }
    onToggleIcon(){
        const currentState = this.state.active
        this.setState({ active: !currentState})
    }
    render(){
        return(
            <div id="nav-icon4" className={this.state.active ? 'open': null} onClick={this.onToggleIcon}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        )
    }
}

export default SiteNavbar;