// IGNORE THIS FILE, WORKING FILE IS NAVBAR_BOOTSTRAP.JSX

import React from 'react';
// import NavbarCategories from '../navbarCategories/NavbarCategories.js'
import Col from 'react-bootstrap/Col'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import './Navbar.css';
// import '../navbarCategories/NavbarCategories.css';
import axios from 'axios'



// Parent Component
class SiteNavbar extends React.Component {
    constructor(){
        super();
        this.state = {
            imgLogo: "https://images.nekojam.com/wp-content/uploads/2019/01/23123239/Nekojam-White-Logo.png",
            homeLink: "https://nekojam.com/"
        }
    }
    render(){
        return(
                <Navbar sticky="top" collapseOnSelect className="bgColor siteHeader" >
                    <Col lg="auto">
                        <Navbar.Brand><SiteLogo homeLink={this.state.homeLink} imgLogo={this.state.imgLogo}/></Navbar.Brand>
                    </Col>

                    <Col className="d-none d-lg-block" >
                        <NavLists />
                    </Col>

                    <Col lg="auto">
                        <HamburgerIcon />
                    </Col>
                </Navbar>
            )
    }
}


// Stateless Component | Logo
class SiteLogo extends React.Component {
    render() {
        return(
                <a href={this.props.homeLink} rel="home">
                    <img src={this.props.imgLogo} alt=""/>
                </a>
            )
    }
}


class NavLists extends React.Component {
    constructor(){
        super();
        this.state = {
            showCategories: 'dont',
            category: null,
            categories: []
        }
        this.onToggleShow = this.onToggleShow.bind( this )
    }

    componentDidMount(){
        axios.get("https://shop.nekojam.com/api/categories/")
            .then(result => {
                this.setState({
                    categories: result.data
                });
            });
    }

    onToggleShow(e){
        const currentState = this.state.showCategories === 'dont' ? 'show' : 'dont'
        let currentCategory = e.target.id
        this.setState({
            showCategories: currentState,
            category: currentCategory
        })
    // Find out whether the states works
    // console.log(currentState)
    // console.log(currentCategory)
    }


    render() {
        console.log('hello from render')
        console.log(this.state.categories)
        const lists = [
        { name: 'Dogs', id: 3},
        { name: 'Cats', id: 0},
        { name: 'Small Pets', id: 4},
        { name: 'Reptiles', id: 1},
        { name: "Today's Deals", id: 2}
        ]
        const navlists = lists.map((list) =>
            <Nav.Item onMouseOver={this.onToggleShow} id={list.id}>
                {list.name}
            </Nav.Item>
            );
        return(
                <Nav className="main-navigation">
                    {navlists}
                    { this.state.categories.length > 0 ? <DropdownCategory category={this.state.categories[this.state.category]}/> : null }
                </Nav>
            )
    }
}

 // this.state.showCategories === 'show' ? <DropdownCategory category={this.state.categories[0]}/> : null


class DropdownCategory extends React.Component {
    render(){
        const categoryList = this.props.category.children.map(list =>
            <li>{list.name}</li>
            );
        return(
            <div className="test" id={'dropdown-' + this.props.category.name}>
            <h1>{this.props.category.name}</h1>
                <ul>
                    {categoryList}
                </ul>
            </div>
            )
    }
}


class HamburgerIcon extends React.Component {
    constructor(){
        super();
        this.state = {
            active: false
        }
        this.onToggleIcon = this.onToggleIcon.bind( this );
    }

    onToggleIcon(){
        const currentState = this.state.active
        this.setState({ active: !currentState})
    }

    render(){
        return(
            <div id="nav-icon4" className={this.state.active ? 'open': null} onClick={this.onToggleIcon}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        )
    }
}


// class SideMenu extends React.Component {
//     constructor(){
//         super();
//         this.state = {
//             active: false
//         }
//         this.showSideMenu = this.showSideMenu.bind( this );
//     }

//     render(){
//         return(
//             <div className="off-canvas-wrapper">
//                 <div>
//                 </div>
//                 <div className="js-off-canvas-overlay is-overlay-fixed is-visible is-closable">
//                 </div>
//             </div>
//             )
//     }
// }


export default SiteNavbar;