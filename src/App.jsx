import React, { Component } from "react";
import axios from "axios";
import { read_cookie } from "sfcookies";
import { global } from "./global.js";

import "./App.css";
import { Frame } from "./productlisting/frame.jsx";
import { Intro } from "./productlisting/intro.jsx";
import { Footer } from "./footer/footer.jsx";
import { Cart } from "./productlisting/cart.jsx";
import SiteNavbar from "./navbar/Navbar_Bootstrap";

import Modal from 'react-bootstrap/Modal'
import "./global.js";



class App extends Component {
  constructor() {
    super();
    this.state = {
      content:
        "Nekojam’s product range of special care and first aid products are selected because of the provided benefit of relief to some of your pet’s most common ailments. Perhaps the most interesting (and popular) would be the range of Jackson Galaxy Solutions, comprising more than 20 different types of relief formulas that may help bring out the best in your pets.",
      categoryProducts: null,
      categoryTitle: null
    };
    this.changeCategory = this.changeCategory.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleShow = this.handleShow.bind(this);

    global.shoppingCart = read_cookie(global.cookieShoppingCartId) || [];
  }


  // Gets props from child component {SiteNavbar}, fetch api, then setState
  changeCategory(newCat){
    // console.log('hello im newCat')
    // console.log(newCat)
    axios
      .get("https://shop.nekojam.com/api/products?category=" + newCat)
      .then(result => {
        // console.log('hello im result')
        // console.log(result)
        this.setState({
          categoryProducts: result.data,
          categoryTitle: newCat
        });
      });
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }


  render() {
    console.log("hello from {App}, render()");
    console.log(this.state.categoryProducts);
    return (
      <div className="App">
        <SiteNavbar categoryInfo={this.changeCategory} modalClick={this.handleShow} />
        <header className="App-header">
        <Intro
          title={this.state.categoryTitle}
          content={this.state.content}
        />
        </header>
        <Frame category={this.state.categoryProducts} />
        <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={this.state.show}
        onHide={this.handleClose}
        >
            <Cart />
        </Modal>
        <Footer />
      </div>
    );
  }
}



export default App;