import React from "react";
import "./filter.css";
import axios from "axios";

export class Filter extends React.Component {
  constructor() {
    super();
    this.state = {
      brands: [],
      stages: []
    };

    axios.get("https://shop.nekojam.com/api/products/brands").then(result => {
      // console.log('hello from filter')
      // console.log(result)
      this.setState({
        brands: result.data
      });
    });

    // Get list of brands only from category selected - how??


    axios
      .get("https://shop.nekojam.com/api/products/attributes/Life Stage")
      .then(result => {
        this.setState({
          stages: result.data
        });
      });

    this.handleBrands = this.handleBrands.bind(this);
    this.handleStages = this.handleStages.bind(this);
  }

  handleBrands(event) {
    console.log("handleBrands");
    console.log(event.target.value)
    this.setState({ value: event.target.value });
  }

  handleStages(event) {
    console.log("handleStages");
    console.log(event.target.value)
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <div className="filter">
        <nav id="sidebar" class="active">
          <ul class="list-unstyled components">
            <li>
              <div class="form-group">
                <label for="sel1">{this.props.label1}</label>
                <select
                  class="form-control"
                  id="sel1"
                  onChange={this.handleBrands}
                  value={this.state.value}
                >
                  <option value="" disabled selected>
                    Select an option
                  </option>
                  {this.state.brands.map(brand => {
                    return <option>{brand.name}</option>;
                  })}
                </select>
              </div>
              <div class="form-group">
                <label for="sel1">{this.props.label2}</label>
                <select
                  class="form-control"
                  id="sel1"
                  onChange={this.handleStages}
                  value={this.state.value}
                >
                  <option value="" disabled selected>
                    Select an option
                  </option>
                  {this.state.stages.map(stage => {
                    return <option>{stage.name}</option>;
                  })}
                </select>
              </div>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}