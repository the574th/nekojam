import React from "react";
import ReactDOM from "react-dom";
import { Product } from "./product.jsx";
import { Filter } from "./filter/filter.jsx";
import axios from "axios";
import "./frame.css";

var sort = "&sort=desc&by=rating";

export class Frame extends React.Component {
  constructor() {
    super();
    this.state = {
      products: []
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    var path = event.target.value;
    axios
      .get("https://shop.nekojam.com/api/products?name=" + path + sort)
      .then(result => {
        this.setState({
          products: result.data
        });
      });
  }


  // Updates { Frame } state when master component { App } instances updates
  componentWillReceiveProps(nextProps) {
    // console.log('Hello I am nextProps')
    // console.log(nextProps.category)
    this.setState({
      products: nextProps.category
    });
  }

  render() {
    console.log("hello from frame, render()");
    console.log(this.state.products);
    return (
      <div className="wrapper">
        <div class="row">
          <div class="col-12 col-md-3" onChange={this.handleChange}>
          {this.state.products.length > 0 ?
            <Filter
              products={this.state.products}
              className="filter"
              label1="SELECT YOUR BRAND"
              label2="LIFE STAGE"
            /> : null
        }
          </div>
          <div class="col-md-9">
            <div class="container">
              <div class="row">
                {this.state.products.map(product => {
                  return (
                    <div
                      class="col-6 col-md-3"
                      onChange={this.handleModal}
                      id={product}
                    >
                      <Product
                        image={product.thumbnailUrl}
                        name={product.name}
                        ratings={product.rating}
                        price={product.price.regular}
                        id={product.id}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Frame />, document.getElementById("root"));