import React from "react";
import "./product.css";
import { IoIosHeartEmpty, IoIosStar, IoMdEye, IoMdCart } from "react-icons/io";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { bake_cookie, read_cookie } from "sfcookies";

import { global } from "./../global.js";

export class Product extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      lgShow: false,
      product: {
        id: "5c04d33be83dab7d9ab29e26",
        name:
          "Nurture Pro Nourish Life Alaskan Salmon Indoor Dry Cat & Kitten Food",
        description: "Description",
        shortDescription:
          "Comes in 4lb & 12.5lb sizes Made with a unique blend of 12 herbs for total wellness Rich in fiber, amino acids & omega oils Contains low levels of ash and magnesium to prevent feline lower urinary tract disease (FLUTD) Suitable for indoor, adult cats",
        brand: {
          id: "5c04cf36e83dab7d9ab27fa1",
          name: "Nurture Pro",
          slug: "nurture-pro"
        },
        images: [
          {
            id: "5c04d33be83dab7d9ab29e25",
            name: "nurture-pro-nourish-life-alaskan-salmon-for-indoor-cat",
            url:
              "https://cdn.shop.nekojam.com/images/original/5c04d33be83dab7d9ab29e25_NurtureProNourishLifeAlaskanSalmonforIndoorCat.jpg",
            thumbnailUrl:
              "https://cdn.shop.nekojam.com/images/thumbnail/5c04d33be83dab7d9ab29e25_NurtureProNourishLifeAlaskanSalmonforIndoorCat.jpg",
            width: 500,
            height: 500
          }
        ],
        attributes: [
          {
            name: "Food Form",
            values: ["Dry Food"]
          }
        ],
        numRatings: 0,
        rating: 0,
        variants: [
          {
            name: "Package Weight",
            values: ["12.5lb", "4lb"]
          }
        ]
      },
      product_variants: [
        {
          id: "5c04d33be83dab7d9ab29e27",
          productId: "5c04d33be83dab7d9ab29e26",
          sku: "E001N271",
          name:
            "Nurture Pro Nourish Life Alaskan Salmon Indoor Dry Cat & Kitten Food",
          image: {
            id: "5c04d33be83dab7d9ab29e25",
            name: "nurture-pro-nourish-life-alaskan-salmon-for-indoor-cat",
            url:
              "https://cdn.shop.nekojam.com/images/original/5c04d33be83dab7d9ab29e25_NurtureProNourishLifeAlaskanSalmonforIndoorCat.jpg",
            thumbnailUrl:
              "https://cdn.shop.nekojam.com/images/thumbnail/5c04d33be83dab7d9ab29e25_NurtureProNourishLifeAlaskanSalmonforIndoorCat.jpg",
            width: 500,
            height: 500,
            alt: ""
          },
          backOrdersAllowed: true,
          attributes: [
            {
              name: "Package Weight",
              value: "4lb"
            }
          ],
          onSale: false,
          price: {
            msrp: 36.6,
            regular: 33,
            sale: 0
          },
          stock: {
            onHand: 2
          },
          manageStock: true,
          stockStatus: "IN_STOCK"
        }
      ]
    };
  }

  handleClose(value) {
    global.shoppingCart.push(value);
    bake_cookie(global.cookieShoppingCartId, global.shoppingCart);
    console.log("Products in cart");
    console.log(global.shoppingCart);
    console.log(read_cookie(global.cookieShoppingCartId));

    this.setState({ lgShow: false });
  }

  handleShow(value) {
    this.setState({ lgShow: true });

    console.log("showing modal for path: " + value);
    axios.get("https://shop.nekojam.com/api/product/" + value).then(result => {
      this.setState({
        product: result.data
      });
    });

    axios
      .get("https://shop.nekojam.com/api/product/" + value + "/variants")
      .then(result => {
        this.setState({
          product_variants: result.data
        });
      });
  }

  render() {
    return (
      <div className="product">
        <div class="pic_container">
          <Modal
            size="lg"
            show={this.state.lgShow}
            onHide={this.handleClose}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <div class="modal-body row">
                <div class="col-md-6">
                  <Modal.Title id="example-modal-sizes-title-lg">
                    <h4>{this.state.product.name}</h4>
                  </Modal.Title>
                  <br />
                  <img
                    src={this.state.product.images[0].url}
                    height="300"
                    width="300"
                  />
                  <hr />
                </div>
                <div class="col-md-6">
                  <span class="modal_price">
                    MSRP: ${this.state.product_variants[0].price.msrp}
                    <br />
                    Our Price: ${this.state.product_variants[0].price.regular}
                  </span>
                  <br />
                  <hr />

                  <Button
                    variant="warning"
                    onClick={() =>
                      this.handleClose(this.state.product_variants[0].sku)
                    }
                  >
                    <IoMdCart class="modal_cart" />
                    &nbsp;Add to Cart
                  </Button>
                </div>
              </div>

              <br />
            </Modal.Header>
            <Modal.Body>
              <div class="modal_description_body">
                <h6>Other Information for</h6>
                <div
                  class="modal_description"
                  dangerouslySetInnerHTML={{
                    __html: this.state.product.description
                  }}
                />
              </div>
            </Modal.Body>
            <Modal.Footer />
          </Modal>

          <button className="wishlist">
            <IoIosHeartEmpty className="heart" />
          </button>
          <br />
          <img
            src={this.props.image}
            size="150"
            width="150"
            className="image"
          />
          <div class="middle">
            <div>
              <button
                type="link"
                onClick={() => {
                  this.handleShow(this.props.id);
                  console.log(this.props);
                }}
                class="text"
              >
                <IoMdEye /> &nbsp; View
              </button>
            </div>
          </div>
          <br />
          <span className="name">{this.props.name} &nbsp;</span>
          <input type="hidden" value={this.props.id} />
          <br />
          <span className="ratings">
            {this.props.ratings} &nbsp;
            <IoIosStar className="star" /> ratings
          </span>
        </div>

        <button class="cart_button">
          <span className="price">Our Price: $ {this.props.price}</span>
          <span class="cart">
            <IoMdCart />
            &nbsp;ADD TO CART
          </span>
        </button>
      </div>
    );
  }
}
