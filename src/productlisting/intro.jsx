import React from "react";
import "./intro.css";

export class Intro extends React.Component {
  render() {
    let title = this.props.title === null ? "Welcome to Nekojam" : this.props.title
    let description = this.props.title === null ? this.props.content : null
    return (
      <div>
        <br />
        <div className="title">{title}</div>
        <div className="content">{description}</div>
      </div>
    );
  }
}