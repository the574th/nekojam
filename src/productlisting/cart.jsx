import React from "react";
import "./cart.css";
import axios from "axios";
import { global } from "./../global.js";
import Button from "react-bootstrap/Button";
import { delete_cookie } from "sfcookies";

export class Cart extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.clearCart = this.clearCart.bind(this);

    this.state = {
      product_variants: []
    };

    global.shoppingCart.map(cartItem => {
      console.log("sku=" + cartItem);

      return axios
        .get("https://shop.nekojam.com/api/products/variants/" + cartItem)
        .then(result => {
          var data = this.state.product_variants;
          data.push(result.data);
          this.setState({
            product_variants: data
          });
        });
    });
  }

  clearCart() {
    delete_cookie(global.shoppingCart);
  }

  render() {
    return (
      <div class="col-12">
        <div class="container">
          <div class="row cart_title_align">
            <br />
            <span class="cart_title">Cart</span>
            <hr />
          </div>

          {this.state.product_variants.map(product_variant => {
            return (
              <div class="row">
                <div class="col-9 ">
                  <div class="row ">
                    <div class="col-12 col-md-3 ">
                      <img
                        src={product_variant.image.thumbnailUrl}
                        width="150"
                        height="150"
                      />
                    </div>
                    <div class="col-12 col-md-4">
                      <div class="row">
                        <span class="cart_product">{product_variant.name}</span>
                        <br />
                      </div>
                      <div class="row">
                        <h6>Standard Weight</h6>
                      </div>
                      <div class="row">
                        <span class="cart_availability">Available</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3">
                  SGD ${product_variant.price.regular}
                </div>
                <br />
              </div>
            );
          })}

          <br />

          {/*
          <Button class="btn btn-warning" onClick={this.clearCart}>
            Clear Cart
          </Button>
      */}
        </div>
      </div>
    );
  }
}