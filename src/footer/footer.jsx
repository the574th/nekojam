import React from "react";
import "./footer.css";
import {
  IoLogoFacebook,
  IoLogoTwitter,
  IoLogoInstagram,
  IoLogoYoutube,
  IoLogoGoogleplus
} from "react-icons/io";

export class Footer extends React.Component {
  render() {
    return (
        <footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col-md-12 footer-height">
                <div class="mb-5 flex-center icons">
                  <a href="https://www.facebook.com/nekojam.shop" class="link">
                    <IoLogoFacebook />
                  </a>

                  <a href="https://twitter.com/nekojamshop" class="link">
                    <IoLogoTwitter />
                  </a>

                  <a href="https://www.instagram.com/nekojam.shop" class="link">
                    <IoLogoInstagram />
                  </a>

                  <a href="https://plus.google.com/+Nekojam" class="link">
                    <IoLogoGoogleplus />
                  </a>

                  <a
                    href="https://www.youtube.com/channel/UC-3TB-BBl7SuMD9sGdzT22w"
                    class="link"
                  >
                    <IoLogoYoutube />
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div class="footer-copyright text-center py-3">
            © 2019 Copyright:
            <a href="https://nekojam.com/" class="link">
              www.nekojam.com
            </a>
            GST: 201714931M
          </div>
        </footer>
    );
  }
}